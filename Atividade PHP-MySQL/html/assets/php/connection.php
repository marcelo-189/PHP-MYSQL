<?php

// Carregamento dos dados
$data = parse_ini_file("config.ini");

// ESCOLHA UM DOS METODOS E APAGUE O OUTRO

// SE PREFERIR UTILIZAR PDO, APAGUE AMBAS E INICIE
// UMA CONEXAO NESTE ARQUIVO

/*-------------------------------------------*/
/* 1. ORIENTADO A OBJETOS                    */
/*-------------------------------------------*/

// Estabelecimento de conexao com o banco de dados
$link = new mysqli($data['host'], $data['username'], $data['password'], $data['database']);

// Checagem de erros na conexao
if($link->connect_error)
	die("Unable to connect to database! Error: " . $link->connect_error);


/*-------------------------------------------*/
/* 2. CHAMADA DE FUNÇOES                     */
/*-------------------------------------------*/

// Estabelecimento de conexao com o banco de dados
$link = mysqli_connect($data['host'], $data['username'], $data['password'], $data['database']);

// Checagem de erros na conexao
if(mysqli_connect_error())
	die("Unable to connect to database! Error: " . mysqli_connect_error());